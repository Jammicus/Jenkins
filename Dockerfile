# Run with docker run --env JAVA_OPTS=-Djenkins.install.runSetupWizard=false -p 8080:8080 -p 50000:50000 <imageID>

FROM jenkins/jenkins

ENV JENKINS_SLAVE_AGENT_PORT 50000
USER root
RUN apt-get update && apt-get install build-essential -y
USER jenkins
COPY executors.groovy /usr/share/jenkins/ref/init.groovy.d/executors.groovy
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
